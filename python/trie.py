#a trie is a prefix search tree e.g. where a position defines the
#key associated with it (just look it up on wikipedia)

class Trie:

    def __init__(self):
        """
        Initialize your data structure here.
        so could have a node with 26 different pointers (one for each), but this
        would be very inefficient in terms of memory usage and so on

        Could be based off frequency of use in English? e.g. a gets a pointer
        but wxyz are all 1 pointer that is later differentiated? List no Ar
        """
        #or
        self.root = TrieNode()
        #this would create a pointer for every letter (memory inefficient)
        #first node needs to not be associated with any letter

    def insert(self, word):
        """
        Inserts a word into the trie.
        :type word: str
        :rtype: void
        """
        focus = self.root
        for character in word:
            if (character not in focus.pointers):
                #create a new node pointed to by focus
                focus.pointers[character] = TrieNode()
            focus = focus.pointers[character]
            #new focus is the next letter in the word (which is a node
            #that has already been created)
        focus.isWord = True # as in it is leaf and last letter of word

    def search(self, word):
        """
        Returns if the word is in the trie.
        :type word: str
        :rtype: bool
        """
        focus = self.root
        for character in word:
            if (character not in focus.pointers):
                return False
            focus = focus.pointers[character]
        return focus.isWord


    def startsWith(self, prefix):
        """
        Returns if there is any word in the trie that starts with the given prefix.
        :type prefix: str
        :rtype: bool
        """
        focus = self.root
        for character in prefix:
            if (character not in focus.pointers):
                return False
            focus = focus.pointers[character]
        return True

class TrieNode:
    def __init__(self):
        #creates a node in the tree
        self.pointers = {} #dict is probs most efficient
        self.isWord = False #so we know if it a whole word or just a part

        """
        for x in range(97, 123):
            self.chr(x) = None
        """
