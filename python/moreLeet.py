class Solution(object):
	def subsets(self, nums):
		ans = []
		nums = sorted(nums)
		self.helperBT(nums, ans, [], 0)
		return ans


	def helperBT(self, nums, ans, potentials, start):
		ans.append(potentials)

		for i in range(start, len(nums)):
			#print([nums[i]]+ potentials)
			self.helperBT(nums, ans, potentials + [nums[i]], i + 1)
		return

	def sortColors(self, nums):
		#do not return anything
		#modify list so all 0s at start 1s in middle and 3s at end

		"""lead = 1; chase = 0 numToGet = 0
		while(chase < len(nums)):
			if(nums[chase] != numToGet):
				for i in range(chase, len(nums)):
					if(nums[i] == numToGet):
						self.swap(chase, i, array)
						will be slow
		#drive all 0s to the left and 2s to the right

		SHIT
		ZeroStart = 0; TwoStart = len(nums) -1
		for i in range(len(nums)):
			print (nums)
			if(nums[i] == 0):
				for j in range(i, ZeroStart, -1):
					#print (i)
					nums = self.swap(j-1, j, nums)
				ZeroStart += 1
		for i in range(len(nums) -1, ZeroStart -1, -1):
			print (nums, ZeroStart, i)
			if(nums[i] == 2):
				for j in range(i, TwoStart):
					nums = self.swap(j+1, j, nums)
				TwoStart -= 1
		print (nums)
		"""

		low, i, high = 0,0,len(nums) -1

		while (i <= high):
			if(nums[i] == 0):
				nums[i] = nums[low]
				nums[low] = 0
				low += 1
			if(nums[i] == 2):
				nums[i] = nums[high]
				nums[high] = 2
				high -= 1
				i -= 1
			i += 1

		#print (nums)


	def swap(self, pos1, pos2, array):
		toMoveIn = array[pos2]
		array[pos2] = array[pos1]
		array[pos1] = toMoveIn
		return array

	def buildTree(self, preorder, inorder):
		return self.helperBuildTree(preorder, inorder, 0, len(inorder)-1,
			0)

	def helperBuildTree(self, preorder, inorder, inStart, inEnd,
						preStart):
		if (inStart > inEnd or preStart > len(preorder)):
			return null

		root = TreeNode(preorder[preStart])

		if(inStart == inEnd):  #reached end of tree
			return root

		inIndex = search(inorder, inStart, inEnd, root.val)
		#finds where root is in the preOrder list to divide it
		#into LHS and RHS values
		root.left = self.helperBuildTree(preorder, inorder, inStart,
			inIndex-1, preStart+1)
		#everything left of root in inorder list is left in tree
		root.right = self.helperBuildTree(preorder, inorder, inIndex +1,
		 inEnd, preStart + inIndex - inStart + 1)

	def search(self, lst, start, end, val):
		for i in range(start, end):
			if(lst[i] == val):
				return i
		return -1

	def search(self, nums, target):
		"""
		:type nums: List[int]
		:type target: int
		:rtype: int
		"""

		#array is pivoted around unknown point, search through
		#array to find point wanted

		#1) find min value- pivot point then undo pivot
		#2) binary search using that
		pivotPoint = self.findPivotPoint(nums)
		low = 0; high = len(nums) - 1

		while(low <= high):
			mid = (low + high) // 2
			rotationMid = (mid + pivotPoint) % len(nums)
			#effectively accounts for overflow we imagine numbers
			#than greater array allows for

			if(nums[rotationMid] == target):
				return rotationMid
			if(nums[rotationMid] < target):
				low = mid + 1
			else:
				high = mid -1
		return -1


	def findPivotPoint(self, nums):
		#easy in o(n) time - can do faster?
		#bin search?
		low = 0; high = len(nums)-1
		while(low < high):
			mid = (low + high)  // 2
			#print (nums[mid])
			if(nums[mid] > nums[high]):
				low = mid + 1
			else:
				high = mid
		#low is the index of the pivot and so the number
		#of places rotated
		#print (nums[low])
		return low

	def maxEleminArray(self, a):
		#fuck u worldpay
		minElem = 0
		maxDiff = a[1] - a[minElem]
		prev = 0
		for num in range(1,len(a)):
			diff = a[num] - a[num-1] + prev
			print (a[num], diff, prev)
			if(diff > 0):
				prev += a[num] - a[num-1]
			elif(diff < 0):
				prev = 0

			if(diff>maxDiff):
				maxDiff = diff

		print (maxDiff)
		return maxDiff

	def connect(self, root):
		"""point each node to its right, if its right doens't
		exist then point it to None
		Surely this is just a bfs?"""

		if(root == None):
			return

		stack = []
		stack.append(root)
		isLast = 1
		level = 1

		while (len(stack) > 0):
			focus = stack.pop(0)
			isLast -= 1

			if(focus.left != None):
				stack.append(focus.left)
			if(focus.right != None):
				stack.append(focus.right)

			print (stack)

			if(isLast > 0):
				focus.next = stack[0]

			if(isLast==0):
				focus.next == None
				level *= 2
				isLast = level



df = Solution()
#df.sortColors([1,2,0,1,2,0,0,2])
#df.buildTree([1,2,4,5,3], [4,2,5,1,3])
#df.maxEleminArray([10,7,6,5,4])


class TreeNode(object):
	 def __init__(self, x):
		 self.val = x
		 self.left = None
		 self.right = None

class TreeLinkNode(object):
	 def __init__(self, x):
		 self.val = x
		 self.left = None
		 self.right = None
		 self.next = None


someNode = TreeLinkNode(5)
someNode.left = TreeLinkNode(3)
someNode.right = TreeLinkNode(2)
df.connect(someNode)
print (someNode.left.next)
