"""You are given a list of non-negative integers, a1, a2, ..., an, and a target,
S. Now you have 2 symbols + and -. For each integer, you should choose one from
+ and - as its new symbol.

Find out how many ways to assign symbols to make sum of integers equal to target S.
"""
import time

class Solution(object):
    def findTargetSumWaysRec(self, nums, S):
        #simple recursion
        #memoize this and we're away
        #print (nums, S)
        if(len(nums)==0):
            if(S==0):
                return 1
            else:
                return 0

        x = self.findTargetSumWaysRec(nums[1:], S-nums[0])
        y =  self.findTargetSumWaysRec(nums[1:], S+nums[0])

        return x+y

    def findTargetSumWays(self, nums, S):
        """need to avoid recalculations as much as possible
        base this on recursive soln but with a map structure
        ((len(nums), target): x+y) """
        if(len(nums)==0):
            return 0
        else:
            return self.helperSumFinder(nums, S, {})

    def helperSumFinder(self, nums, S, map):
        #print (map)
        memo = (len(nums), S)
        if(memo in map):
            return map[memo]

        elif(len(nums)==0):
            if(S==0):
                return 1
            else:
                return 0

        else:
            x = self.helperSumFinder(nums[1:], S-nums[0], map)
            y =  self.helperSumFinder(nums[1:], S+nums[0], map)
            map[memo] = x+y
            return x+y

    def canFinish(self, numCourses, prerequisites):
        """need to make a adjacency list of this, and
        get rid of the duplicates and other good stuff
        """
        adjList = self.makeAdjList(numCourses, prerequisites)
        degree = self.findinDegress(prerequisites, numCourses)
        queue = self.makeQueue(degree)
        count = len(queue)

        while(len(queue) > 0):
            course = queue.pop()
            #print (course)
            for i in range(len(adjList[course])):
                #print (i) is 0 start at end
                pointer = adjList[course][i]
                degree[pointer] -= 1 #where node is pointing to now
                if(degree[pointer]==0):
                    queue.append(pointer)
                    count+=1

        if(count==numCourses):
            return True
        else:
            return False

    def makeAdjList(self, numCourses, prerequisites):
        #pres are always pairs
        #indegrees - number of arcs leading to a vertex
        #outdegree - number of arcs leading away from a vertex
        adjList = []
        for x in range(numCourses):
            adjList.append([])
        for i in range(len(prerequisites)):
            adjList[prerequisites[i][0]].append(prerequisites[i][1])
            #e.g. [0] -> 1 etc
        return adjList

    def findinDegress(self, prerequisites, numCourses):
        inDegree = []
        for x in range(numCourses):
            inDegree.append(0)
        for i in range(len(prerequisites)):
            inDegree[prerequisites[i][1]] += 1
            #if pointed to multiple times
        return inDegree
    def makeQueue(self, degees):
        queue = []
        for i in range(len(degees)):
            if(degees[i]==0):
                queue.append(i)
            #npthing pointing away form node
        return queue



obj = Solution()
print(obj.canFinish(4, [[1,0],[0,4]]))
