#!/usr/bin/env python3

class Solution(object):
    def groupAnagrams(self, strs):
        """
        :type strs: List[str]
        :rtype: List[List[str]]
        """
        ans = []
        myDict = {}

        for word in strs:
        	charList = sorted(word)
        	sortedWord = "".join(charList)

        	if(sortedWord in myDict):
        		subList = myDict.get(sortedWord)
        		subList.append(word)
        	else:
        		myDict[sortedWord] = [word]

        #for val in myDict.itervalues():
        #	ans.append(val)

        return myDict.values()

    def singleNumber(self, nums):
    	#input is a list of ints- find the non duplicate

    	#2 pointers?
    	i = 0; j = 0

    	"""
    	#pythonic way lol
    	for n in nums:
    		if nums.count(n) == 1:
    			return n

    	return reduce(lambda x, y: x ^ y, nums)
    	#which is the same as
    	"""
    	return helperfun(nums)

    def merge(self, intervals):

    	intervals = sorted(intervals, key = lambda x: x.start)
    	#sort based off the first interval
    	length = len(intervals)
    	if(length == 0):
    		return []

    	ansList = []
    	ansList.append[0]
    	for i in range(1,n):
    		firstElem = ansList[-1]
    		nextElem = intervals[i]
    		if nextElem.start > firstElem.end:
    			ansList.append(nextElem)
    		else:
    			ansList[-1].end = max(firstElem.end, nextElem.end)
    	return ansList


    def helperfun(self, nums):
    	i = 0
    	for n in nums:
    		i ^= n
    		#equiv to A = A ^ B
    		print (i)
    	return i


    def searchRange(self, nums, target):
        #must be o(logn) - elements are sorted- its a
        #binary search with a twist

        bajo = 0; alto = len(nums) - 1
        medio = alto // 2 #integer division

        while(bajo <= alto):
            medio = (alto + bajo) // 2
            testVal = nums[medio]
            print (testVal)
            if(testVal == target):
                bajo = medio; alto = medio
                while (bajo > 0 and nums[bajo-1] == target):
                    bajo -= 1
                    print ("bajo esta: " + str(bajo))
                while (alto < len(nums)-1 and nums[alto + 1] == target):
                    alto += 1
                    print ("alto esta: " + str(alto))
                return [bajo, alto]

            elif (testVal < target):
                bajo = medio + 1
            else:
                #test val is bigger
                alto = medio - 1
        return [-1, -1]

    def strangerThings(self, FourdigPass):
        for i in range(10):
            for x in range(10):
                for j in range(10):
                    for k in range(10):
                        if(i * 1000 + x * 100 + j * 10 + k == FourdigPass):
                            return "found the pass"
        return "nope"

    def exist(self, board, word):
        #list[list[string]]
        #return boolean
        #print (len(board[0]))
        for h in range(len(board)):
            for w in range(len(board[0])):
                #0(n^3) yay
                #print (h, w)
                #print (board[h][w])
                #use the second help func
                #can avoid using index by stripping first letter of word
                if (self.existHelp(board, word, h, w) == True):
                    return True
        return False

    def existHelp(self, board, word, posY, posX):
        #this function searches the ajacent cells to see is they
        #contain the next letter in the word
        #to avoid repeats could use a dictionary made up of tuples

        if(len(word)==0):
            #print ("this is true")
            return True

        if(posX >= len(board[0]) or posX < 0 or posY >= len(board) or
            posY < 0 or word[0] != board[posY][posX]):
            return False  #is out of bounds

        print (word, posY, posX)


        firstLetterStore = board[posY][posX]
        board[posY][posX] = "!" #i.e. already visited

        if(self.existHelp(board, word[1:], posY - 1, posX) or
    self.existHelp(board, word[1:], posY + 1, posX) or
    self.existHelp(board, word[1:], posY, posX - 1) or
    self.existHelp(board, word[1:], posY, posX + 1)):
            return True

        #if there's nothing but there might still be a soln need to 'reset'
        board[posY][posX] = firstLetterStore
        return False

    def inorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        #return self.buildList(root, []) trivial recursive
        lst = []
        #try iterative
        stack = []  #stack of treeNodes
        stack.append(root)
        focus = root

        while(len(stack) != 0):
            if(focus.left != None):
                stack.append(focus)
                focus = focus.left

            else:
                focus = stack.pop()
                lst.append(focus.val)
                focus = focus.right
        #reached the end of LHS of tree

        return lst



    def buildList(self, root, lst):
        if(root == None):
            return

        self.buildList(root.left, lst)
        lst.append(root.val)
        self.buildList(root.right, lst)
        return lst

    #Given an array of integers and an integer k, you need to find the total
    #number of continuous subarrays whose sum equals to k.
    def subarraySum(self, nums, k):
        #could just use 2 pointers?
        """j,i,ans = 0,1,0
        while (j < len(nums)):
            subArSum = nums[j] #must include first term
            if(subArSum == k):
                ans+=1

            #print (subArSum)
            while (i < len(nums)):
                #subArSum <= k and :: includes -ve nums - will have to
                #scan whole array each time (complexity a lot)

                #brute force approach times out, need to save results to
                #avoid recalculate stuff is sum -k been done before then
                #add on values
                # use a int to int dictionary to store num solns at each
                #possible iteration of the array
                subArSum += nums[i]
                i+=1
                if (subArSum == k):
                    ans += 1
            j+=1;i = j+1
            #moves the sliding window along
        print (ans)
        return ans"""
        subSumStore = {0:1}
        subSum = result = 0

        for i in range(len(nums)):
            subSum+=nums[i]
            if ((subSum - k) in subSumStore):
                result+=subSumStore[subSum-k]
                #adds val associated to res (will usually be 0)
            if(subSum not in subSumStore):
                subSumStore[subSum] = 1
                #adds new key in dict of sum (i.e. sum of all preceeding vals)
                #if this + k = future sum then futureSum - this = k i.e.
                #the values between this and f are a subset equalling k
            else:
                subSumStore[subSum] += 1
                #increments already calced sum (e.g. k=2 4,-2,-1,-1,2)
                #would need to += 2 (all together and the alone 2)
        #print (result)
        return result


        #keep a sum and save every integer to the dictionary if sum - k is in
        #dictionary then we have a new solution
    def pascalsTriangle(self, numRows):
        #this is a bit of a dp question methinks
        """n,k = n-1,k-1 + n-1,k"""
        if(numRows == 0):
            return []

        triangle = [[1]]

        for n in range(1, numRows):
            thisRow = []
            thisRow.append(1)
            for k in range(1, n):
                #row 0 has 1 elem etc
                #print (n,k)
                thisRow.append(triangle[n-1][k-1] +
                           triangle[n-1][k])

                #only need to calculate nums between 1..1
            thisRow.append(1)
            #first and last elem always 1
            triangle.append(thisRow)
            #print (triangle)
        return triangle

    def generate(self, numRows):
        x=[]
        row=[1]

        for i in range(numRows):
            x.append(row)
            row=[x+y for x,y in zip([0]+row,row+[0])]

        return x

    def reverseWords(self, s):
        newString = ""
        for wrd in s.split(" "):
            wrd = self.reversoTron(wrd)
            newString += (wrd + " ")
        #print (newString)
        return newString[:len(newString)-1]

    def reversoTron(self, word):
        #best to not use recursion
        """if(len(word) == 1):
            return word
        return self.reversoTron(word[1:]) + word[0]"""
        #non recursive
        return word[::-1]
        #extended slice syntax count down from right to left

    def swapLetters(self, a, b, wrd):
        #this doesn't work -> think why
        copy = wrd[a]
        wrd[a] = wrd[b]
        wrd[b] = copy
        return wrd

    def maxProfit(self, prices):
        """this is a defintie dp problem
        #1) can this be solved recursively
        options are a)buy the stock and don't buy any tomorrow
        2) no comprar la accione sino comprar la accione de
        manana """
        #return max(self.mpRec(prices[1:], None),
        #            self.mpRec(prices[1:], prices[0]))
                    #buy or not only options
        sell, prevSell = 0,0
        buy, prevBuy = -10000, -10000 #elimates chance of selling nothing
        for elem in prices:
            prevBuy = buy
            buy = max(prevSell-elem, prevBuy)
            #stick or twist
            prevSell = sell
            sell = max(prevBuy+elem, prevSell)
            #print (buy, sell)
        return sell

    def mpRec(self, prices, stock):
        #cd = cooldown - boolean
        #this needs to be memoized
        print (prices)

        if(len(prices)==0):
            return 0;

        if(stock!=None):
            salePrice = prices[0] - stock
            return max(self.mpRec(prices[1:], stock),#do nothing
                salePrice + self.mpRec(prices[2:], None))#sell
                #if sold last time reselling does nothing
        else: #we have stock
            return max(self.mpRec(prices[1:], stock),#do nothing
                self.mpRec(prices[1:], prices[0])) #buy (allows double buying)
                #if sold last time reselling does nothing

    def maxLenthSubArray(self, A, B):
        """find the maximum length of a subarray that appears
        in both arrays. Need to find the longest common suffix
        before all pairs of prefixes in the strings

        This is correct just getting timeout on last challenge"""
        assert len(A) > 0 and len(B) > 0

        dpTable = [[]]
        dpDict = {}
        #table is faster
        dpTable = [0] * len(A) + 1

        maxVal = 0
        for i in range(len(A)+1):
            #dpTable.append([])
            for j in range(len(B)+1):
                #dpTable[i].append(0)
                if (i==0 or j==0):
                    #dpTable[i][j] = 0
                    #length = 0 if a subarray is 0
                    dpDict[(i,j)] = 0
                else:
                    if (A[i-1] == B[j-1]):
                        #there is a common prefix
                        #dpTable[i][j] = dpTable[i-1][j-1] + 1
                        dpDict[(i,j)] = dpDict[(i-1,j-1)] + 1
                        #increment saved val by 1 (new prefix)
                        maxVal = max(dpDict[(i,j)], maxVal)
                        #but is it the longest
                    else:
                        dpDict[(i,j)] = 0
        return maxVal

    def knightProbability(self, N, K, r, c):
        #rType: float
        """each position on the board will have its own
        probability that must be calculated by then can
        just be retained
        Could just calculate a denominator and numerator
        then divide them, still need to memoize otherwise
        this will just get ridiculous """
        moves = ((2,1), (1,2), (2,-1), (1,-2),
        (-2,1), (-1,2), (-2,-1), (-1,-2))
        #all potential moves (in pairs)
        board=[[]] #need this outside of func

        timesOff=0
        for x in range(K):
            #could count all times we move off then 1-it
            for move in moves:
                if(self.isOutofBoard(move[0] + r, move[1] +c)):
                    timesOff+=1
                board[r+move[0]][c+move[1]] = timesOff
                #prob of any cell is just sum of 'danger zones' it
                #could hypothetically get to


        #then divide the tuple or something
        board[n,c] = dem / 8
        return board[n,c]

    def isOutofBoard(self, row, column):
        if (r >= N-1 or c >= N-1 or r < 0, or c < 0):
            return True
    else:
        return False

someList = [1, 2, 3, 0, 2]

#print (someList[2][0])
obj = Solution()
#obj.searchRange(someList, 1)
#print(obj.exist(someList, "BCESEEE"))
#obj.subarraySum(someList, 2)
print(obj.maxProfit(someList))
print (obj.maxLenthSubArray(someList, [8,7,1,2,3]))


class Interval(object):
	def __init__(self, s=0, e=0):
		self.start = s
		self.end = e

class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
