
import java.util.HashMap;
import java.util.Stack;

class compression {

  /*
  In this exercise, you're going to decompress a compressed string.

Your input is a compressed string of the format number[string] and the
decompressed output form should be the string written number times.
For example:

The input

3[abc]4[ab]c

Would be output as

abcabcabcababababc

Lotta problems numbers more than 1 digit fail
*/
  String decompress(String input){
    /*loops through matching the leftmost brcae with its corresponging elem
    i.e. finds each bracketed block and each one to the stack
    3[ab2[gv]] = (2gh, 3ab2[gv])
    */
    Stack<String> orderStack = new Stack<>();
    input = this.sortInput(input);

    for(int i =0; i < input.length(); i++){
      if(input.charAt(i) == '['){
        int forwards = 1;
        for(int j = i+1; j < input.length(); j++){
          if(input.charAt(j) == '[')
            forwards += 1;
          if(input.charAt(j) == ']'){
            forwards -= 1;
            if(forwards == 0){
              //i.e. this is the matching brace
              String decomp =
                input.substring(i-1,i).concat(input.substring(i+1, j));

              if(Character.isDigit(decomp.charAt(0)) == false){
                decomp = decomp.substring(1, decomp.length());
                decomp = "1".concat(decomp);
              }
              System.out.println("inp " + decomp);
              //System.out.println("inp " + decomp);
              orderStack.push(decomp);
            }
          }
        }
      }
    }
    if(orderStack.empty())
      return input;

    String answer = this.processStack(orderStack);
    return answer;
  }

  String extract(String compressed){
    //keep it simple
    /*This is still o(n) somehow Procedure:
    input cannot have two compressions on the same 'level'
    input cannot have no number before a compression

    Fairly simple finds a number then its corresponding letter group
    and adds them to a stack (using custom data type). Therefore the last
    added element e.g. the deepest letters are processed fist
    */
    int index = 0;
    Stack<NumString> orderStack = new Stack<>();
    while(index < compressed.length() &&
          compressed.charAt(index) != ']'){
        //first closing bracket is end of what we care about
            if(Character.isDigit(compressed.charAt(index))){
              //these must be letters to decompress
              int factor = getNum(compressed, index);
              index += 1 + String.valueOf(factor).length();
              //skips the bracket and the numbers

              String letterClump = "";
              int indexRef = index;

              while(Character.isLetter(compressed.charAt(indexRef))){
                letterClump += compressed.charAt(indexRef);
                indexRef ++;
              }

              orderStack.push(new NumString(factor, letterClump));
              System.out.println(factor + " " + letterClump);
              index = indexRef;
            }
            index ++;
          }
        String ans = betterProcessStack(orderStack);
        System.out.println(ans);
    return ans;
  }
  private int getNum(String sub, int i){
    int subTumes = 0;
    while(Character.isDigit(sub.charAt(i))){
      subTumes = subTumes * 10 + Character.getNumericValue(sub.charAt(i));
      //this is really irratatingly clever
      i++;
    }
    return subTumes;
  }

  private String sortInput(String inp){
    if(Character.isDigit(inp.charAt(0)) == false){
      return "1[".concat(inp).concat("]");
    }else{
      return inp;
    }
  }

  String processStack(Stack<String> theStack){
    /*Works through the stack building the string from the inside out
    there is a lot of sillyness because the input from above was a bit weird
    So first element of stack the 'deepest' bracketed block. This is then
    decompressed and appended to the next 'deepest' which is then all
    decompressed. This is then built up recursively
    */

    String previousDecompression = "";
    int lengthPrev = 0;

    while(theStack.empty() != true){

      String focus = theStack.pop();
      int numberTimes = Character.getNumericValue(focus.charAt(0));

      System.out.println("focus is: " + focus + " " + previousDecompression);

      String actualMessage =
          focus.substring(1, this.findMessageLen(focus));

        //System.out.println(actualMessage + " " + numberTimes);
          //+2 to compensate for brackets
      actualMessage = actualMessage.concat(previousDecompression);
      //System.out.println(actualMessage + " " + numberTimes);

      previousDecompression = (testDecomp(numberTimes, actualMessage));
      lengthPrev = previousDecompression.length();
    }
    System.out.println("final ans: " + previousDecompression);
    //done and done
    return previousDecompression;
  }
  /*
  Separates elements of the stack into the part that hasn't already been
  copied and the part that has so the decompressed part can replace the
  undecompressed part e.g. 2bc3[ac] returns 3 so it can be split into
  2bc += acacac
  */
  private String betterProcessStack(Stack<NumString> valuesStack){
    //similar to above but less silly
    String previousDecompression = "";
    while(!valuesStack.empty()){
      NumString focus = valuesStack.pop();

      String toDecompress = focus.getString();
      toDecompress += previousDecompression;
      //e.g. 4ab3[c] -> 4 ("ab" + = "ccc")

      String decompressed = this.testDecomp(focus.getNum(), toDecompress);
      previousDecompression = decompressed;
    }
    return previousDecompression;
  }

  int findMessageLen(String message){
    //System.out.println("message is: " + message);
    for (int i = 1; i< message.length(); i++){
      if(message.charAt(i) == '['){
        return i -1;
      }
    }
    return message.length();
  }


  String testDecomp(int numTimes, String repeat){
    if(numTimes == 1){
      return repeat;
    }else{
      return repeat + testDecomp(numTimes - 1, repeat);
    }
  }
  private class NumString{
    //helps store values
    private int num;
    private String letters;
    NumString(int a, String b){
      this.num = a;
      this.letters = b;
    }
    int getNum(){
      return this.num;
    }
    String getString(){
      return this.letters;
    }
  }
  private void cmon() {
    int[] ar = new int[] {1,2,3,4}; 
  }

}
