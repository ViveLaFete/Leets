package com.stuff;

import java.util.HashMap;

public class Trees {
    //smoke trees 4 lifeeeeeeee
    /*
    Operations supported are searching traversal insertion and deletion

    Need to make this a balanced search tree by using ordering methods
     */
    treeStructure searchTree(treeStructure focus, int x){
        //HashMap

        if(focus == null)
            return null;
        if(focus.value == x)
            return focus;
        if(x < focus.value)
            return searchTree(focus.left, x);
        else
            return searchTree(focus.right, x);
    }
    treeStructure findMin(treeStructure root){
        //and right for the max element
        if(root == null)
            return null;

        treeStructure min = root;
        while(min.left != null){
            min = min.left;
        }
        return min;
    }
    void insertion(treeStructure l, int val, treeStructure parent){
        //this is a constant time operation

        if(l==null){
            treeStructure p = new treeStructure();
            p.value = val;
            p.left = p.right = null;
            p.parent = parent;

            if(val < parent.value)
                parent.left = p;
            else
                parent.right = p;
            return;
        }
        if(val < l.value)
            insertion(l.left, val, l);
        else
            insertion(l.right, val, l);
    }
    //deletion is harder
    void delete (int val, treeStructure root){
        treeStructure doomed = searchTree(root, val);

        if(doomed.left == null && doomed.right == null)
            doomed.parent = null; //cya
        if(doomed.left == null && doomed.right != null){
            doomed.parent = doomed.right;
        }
        if(doomed.left != null && doomed.right == null){
            doomed.parent = doomed.left;
        }
        //if both nodes active then replace this with the minimum node in the right
        //subtree
        else{
            treeStructure successor = (findMin(doomed.right));
            doomed.parent = successor;
        }
        return;
    }
    void sort(){

    }

}
class treeStructure {
    int value;
    treeStructure parent;
    treeStructure left;
    treeStructure right;
}
