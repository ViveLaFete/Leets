using System;

namespace googleProbs
{
    class miscProbs
    {
        /*
        Filling up water in an array where each height represents a bar which
        can be filled e.g. 3 1 1 4 means (3-1) + (3-1)
        */
        public int findAmountWater(int[] heights){
          //this can be simplified by finding the maxLeft heights and the
          /*max right heights
          Find maximum height of bar from the left end upto an index i in the
           array left_max
.
Find maximum height of bar from the right end upto an index i in the array
right_max
.
Iterate over the height\text{height}height array and update ans:
Add min(max_left[i],max_right[i])−height[i]
to ansansans
*/
          if(heights.Length == 0)
            return 0; 

          int[] adelante = makeForwardArray(heights);
          int[] espalda = makeBackwardArray(heights);
          int ans = 0;

          for (int iter = 0; iter < heights.Length; iter++)
          {
            ans += Math.Min(adelante[iter], espalda[iter]) - heights[iter];
          }
          Console.WriteLine(ans);
          return ans;
        }

        private int[] makeForwardArray(int[] heights){
          //record max left facing heights
          int[] lRecords = new int[heights.Length];
          int initialMax = heights[0]; lRecords[0] = initialMax;

          for(int i = 1; i < heights.Length; i++)
          {
            initialMax = Math.Max(initialMax, heights[i]);
            lRecords[i] = initialMax;
          }
          return lRecords;
        }
        private int[] makeBackwardArray(int[] heights)
        {
          int[] rRecords = new int[heights.Length];
          int initialMax = heights[heights.Length-1];
          rRecords[heights.Length-1] = initialMax;

          for(int i = heights.Length-2; i >= 0; i--)
          {
            initialMax = Math.Max(initialMax, heights[i]);
            rRecords[i] = initialMax;
          }
          return rRecords;
        }

        public int naiveSoln(int[] heights){
          //the problem with this is the issue of repeated subproblems

          int left, right;
          int total = 0;

          for (left = 0; left < heights.Length-1; left++)
          {
            int hght = heights[left];
            right = left + 1;
            while(right < heights.Length && heights[right] < hght)
            {
              right++;
            }
            //int waterLevel = Math.Max(hght, heights[right]);
            for(int iLeft = left + 1; iLeft < right - 1; iLeft++)
            {
              int waterLevel = Math.Max(hght, heights[right]);
              total += (waterLevel - heights[iLeft]);
              Console.WriteLine(total);
            }
          }
          return total;
        }
    }
}
