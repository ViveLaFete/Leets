﻿using System;

namespace Leetcode
{
	public class Binomial
	{
		public Binomial ()
		{
		}
		public int fibn(int n){
			if(n==0)return 0;
			if (n == 1)
				return 1;

			return fibn (n - 1) + fibn (n - 2);
		}
		public int binomial_coefficient(int n, int m){
			int i, j;

			/*
			 *(n - k abv 0) = 1 (1 way to choose 0 things from a set)
			 * m | 1 = m e.g. (k | k) 1 complete set 
			 * n || k = n-1||k-1 + n-1||k
				*/
			int[,] bc = new int[n+1, n+1]; //could be long 
			//stores the bin coefficients 

			for(i = 0; i<=n; i++){
				bc[i,0] = 1;
				//when n = 0 ans is 1 
			}
			for(j =0; j<=n; j++){
				bc[j, j] = 1;
				//when m = x = n ans is 1
			}

			for(i =1; i <= n; i++){
				for(j =1; j < i; j++){
					bc[i, j] = bc[i-1, j-1] + bc[i-1, j];
				}
			}
			//Console.WriteLine (bc [n, m]);
			return bc[n, m];
		}
	}
}

