﻿using System;using System.Collections.Generic;

enum Index {
	GOOD, BAD, UNKNOWN
}

public class Solution {
	Index[] memo;

	public bool canJumpFromPosition(int position, int[] nums) {
		if (memo[position] != Index.UNKNOWN) {
			return memo[position] == Index.GOOD ? true : false;
		}

		int furthestJump = Math.Min(position + nums[position], nums.Length - 1);
		for (int nextPosition = position + 1; nextPosition <= furthestJump; nextPosition++) {
			if (canJumpFromPosition(nextPosition, nums)) {
				memo[position] = Index.GOOD;
				return true;
			}
		}

		memo[position] = Index.BAD;
		return false;
	}

	public bool canJump(int[] nums) {
		memo = new Index[nums.Length];
		for (int i = 0; i < memo.Length; i++) {
			memo[i] = Index.UNKNOWN;
		}
		memo[memo.Length - 1] = Index.GOOD;
		return canJumpFromPosition(0, nums);
	}
	public Boolean bottomUp(int[] nums){
		Index[] memo = new Index[nums.Length];
		for (int i = 0; i < memo.Length; i++) {
			memo[i] = Index.UNKNOWN;
		}
		memo[memo.Length - 1] = Index.GOOD;

		for (int i = nums.Length - 2; i >= 0; i--) {
			int furthestJump = Math.Min(i + nums[i], nums.Length - 1);
			for (int j = i + 1; j <= furthestJump; j++) {
				if (memo[j] == Index.GOOD) {
					memo[i] = Index.GOOD;
					break;
				}
			}
		}

		return memo[0] == Index.GOOD;
	}
	public bool wordBreak(string s, IList<string> wordDict){
		bool[] ar = new bool[s.Length + 1];
		//boolean array to store which indexes have complete words 
		//good example of DP

		ar [0] = true;

		for(int i = 1; i <= s.Length; i++){
			for(int j=0; j < i; j++){
				if(ar[j] && wordDict.Contains(s.Substring(j,i))){
					//is the start (j) the end of a word? And is the end(i)
					//actually a word at all? 
					ar [i] = true;
					break;
				}
			}
		}
		return ar [s.Length];
	}
}