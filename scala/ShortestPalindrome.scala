package learning

import scala.collection.mutable

class ShortestPalindrome {
    def shortestPalindrome(s: String): String = {
        var results = mutable.HashSet[String]()
        for (i <- 0 until s.length-1) {
            if (s(i).equals(s(i+1))) {
                var apres = i+1
                var avant = i
                while (avant >= 0 && apres <= s.length - 1 && s(avant).equals(s(apres))) {
                    //if (apres == s.length - 1)
                        //the palindrome is longer on the LHS...
                        //not a solution would have to add whole string on again
                    avant -= 1
                    apres += 1
                }
                if (apres != s.length-1) results.add(s.substring(apres).reverse)
            }
        }
        if (results.isEmpty) s.reverse
        else results.min
    }
    //KMP alogirithm
    /**
      * searches for occurrences of a "word" W within a main "text string" S by employing the observation that when a
      * mismatch occurs, the word itself embodies sufficient information to determine where the next match could begin,
      * thus bypassing re-examination of previously matched characters.
      * @param searchText
      * @param word
      * @return
      */
    def KMP_Search(searchText: String, word : String) : String = {
        val partialSearchTable : Array[Int] = getPartialSearchTable(word)
        //finds a table that contains indices of where to start backtracking from if current match ends in mismatch
        //to avoid repeatedly re-searching the same characters
        var numberPositions = 0
        var positionsWhereWordFound = List[Int]()
        var j,k = 0
        while (j < searchText.length) {
            if (word(k).equals(searchText(j))) {
                //found match increment indices- do we still have match
                j += 1
                k += 1
                if (k == word.length) {
                    //found occurence of word
                    positionsWhereWordFound :::= List(j-k)
                    numberPositions += 1
                    k = partialSearchTable(k)
                }
            } else {
                k = partialSearchTable(k)
                if (k < 0) {
                    j += 1
                    k += 1
                }
            }
        }
        println(numberPositions)
        for (x <- positionsWhereWordFound) {println(x)}
        ""
    }

    /**
      * Gets a tables that means the algorithm does not match any character of S more than once. Having checked some seg
      * ment of the main string against an inital segment of the pattern, we know where a new potential match could begin
      * that is able to reach the current position. Pre-searches the pattern to find "fallback positions"
      *
      * For each pos in text find the length of the longest possible intial segment of text leading up to that position,
      * other than text[0]. So T[i] is the length of the longest possible proper initial segment of W, which is also
      * a segment of the substring ending at W[i-1].
      * @param text
      * @return
      */
    def getPartialSearchTable(text : String) : Array[Int] = {
        var candidate = 0 //index in text of the next character in the current candidate substring
        var pos = 1
        val table = new Array[Int](text.length+1)

        table(0) = -1
        while (pos < text.length) {
            if (text(pos).equals(text(candidate))) {
                table(pos) = table(candidate)
                pos += 1
                candidate += 1
            } else {
                table(pos) = candidate
                //no match- set to index where pattern isn't repeated
                //e.g. ABCDABD table(6) is 2 because AB is repeated and wouldn't need to search from there again

                //would end up at index 0 (-1) if set to table(0) or at same position as pos
                //try and shorten match string until revert
                candidate = table(candidate)
                while (candidate >= 0 && text(pos) != text(candidate)) {
                    candidate = table(candidate)
                }
                pos += 1
                candidate += 1
            }
        }
        table(pos) = candidate
        table
    }
    def shortestPalindromeWorks(palin : String) : String = {
        val revPalin = palin.reverse
        val table = getPartialSearchTable(palin + "|" + revPalin)
        //the last element in the table is the index that contains where the palindrome stops matching
        //unless the end of the word is "palindromic" then would have to add entirety to start
        palin.substring(table(table.length - 1)).reverse + palin
    }
}
