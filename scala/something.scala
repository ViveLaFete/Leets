package learning

class something {
    def findLHS(numbers: Array[Int]): Int = {
            if (numbers.length == 0) return 0
            val sortedNums = numbers.sortWith(_ < _)
            //var iter = 0
            var lowerCount = 1
            var upperCount = 0
            var lowerCountStart = sortedNums(0)
            var higherCountStart = 0
            var maxListLength = 0
            var isDiff = false
            for (iter <- 1 to numbers.length - 1) {
                val count1Diff = sortedNums(iter) - lowerCountStart
                val count2Diff = sortedNums(iter) - higherCountStart
                if (count1Diff == 0) {
                    lowerCount += 1
                } else if (count1Diff == 1) {
                    //sequence has incrememted
                    isDiff = true
                    lowerCount += 1
                    higherCountStart = sortedNums(iter)
                    upperCount += 1
                } else if (sortedNums(iter) - lowerCountStart > 1) {
                    //increased by 2 or more from original start
                    if (isDiff) {
                        maxListLength = math.max(maxListLength, lowerCount)
                    }
                    //count1 cannot be incrememted any higher set count 1 to be the lower
                    if (count2Diff == 1) upperCount += 1
                    lowerCount = upperCount
                    lowerCountStart = higherCountStart
                    //reset higher count to be new higher number
                    higherCountStart = sortedNums(iter)
                    upperCount = 1
                }
            }
            if (isDiff) {
                maxListLength = math.max(maxListLength, lowerCount)
            }

            maxListLength
    }
    def findLHS2(nums : Array[Int]) :Int = {
        if (nums.length == 0) return 0

        val numbers = nums.sortWith(_ < _)
        var lowCountStart = 0
        var highCountStart = 0
        var res = 0

        for (currentPosition <- 1 until numbers.length) {
            val currentValue = numbers(currentPosition)
            //use current index moving through array as the length of the longest subsequence
            if (currentValue - numbers(lowCountStart) > 1) {
                lowCountStart = highCountStart
            }
            if (currentValue != numbers(currentPosition - 1)) {
                highCountStart = currentPosition
            }
            if (currentValue - numbers(lowCountStart) == 1) {
                res = math.max(res, currentPosition - lowCountStart + 1)
            }
        }
        res
    }
}
